# TASK MANAGER

## DEVELOPER INFO

**NAME:** Sergey Kuzmin

**EMAIL:** winterfield91@yandex.ru

## SYSTEM INFO
**OS**: Linux Mint 21.1 Vera

**JDK**: Java 1.8

**RAM**: 16GB

**CPU**: i5

## BUILD PROJECT
````
mvn clean install
````

## RUN PROJECT
````
cd ./target
java -jar ./task-manager.jar
````
